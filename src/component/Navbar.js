import React from "react";
import "../App.css";

// import './style/css/styles.css'
// import './style/js/scripts'
function Navbar(props) {
  const { judul } = props;
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div className="container-fluid">
          <a className="navbar-brand js-scroll-trigger" href="#about">
            {judul}
          </a>
          <button
            className="navbar-toggler "
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse navbar-collapse justify-content-end "
            id="navbarNav"
          >
            <ul className="navbar-nav ms-auto me-5">
              <li className="nav-item">
                <a
                  className="nav-link js-scroll-trigger"
                  aria-current="page"
                  href="#about"
                >
                  About
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="#projects">
                  Pengalaman
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="#karya">
                  Karya
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="#footer">
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
