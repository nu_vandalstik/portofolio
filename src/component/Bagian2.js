import logo1 from "./assets/img/ipad.png";
import "../App.css";
export default function Bagian2(props) {
  const { siapa } = props;

  const getAge = (birthDate) =>
    Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e10);
  const UsiaSekarang = getAge("1997-10-07");

  const skill = ["MongoDB", "Express JS", "React JS", "NodeJS"];
  const showSkill = skill.join(", ");
  return (
    <div>
      <section className="about-section text-center" id="about">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 mx-auto">
              <h2 className="text-danger mb-4">Tentang Saya </h2>
              <p className="text-light fw-normal ">
                {siapa} <br /> {showSkill}{" "}
              </p>

              <p className="text-light fw-normal">
                Berpengalaman Menggunakan PHP 7 & MySqli MariaDB <br />{" "}
                Menggunakan Menggunakan framework CodeIgniter V.3{" "}
              </p>
              <p className="text-light "></p>

              <p className="text-light fw-normal">
                Usia Saya Sekarang {UsiaSekarang}
              </p>
            </div>
          </div>
          <img className="img-fluid mb-5" src={logo1} alt="..." />
        </div>
      </section>
    </div>
  );
}
