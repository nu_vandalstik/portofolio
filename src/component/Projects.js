// import './style/css/styles.css'
// import './style/js/scripts'
import "../App.css";

// import Logo1 from "./assets/img/logo1.jpg";
import logo2 from "./assets/img/keahlian.jpg";
import logo3 from "./assets/img/sertifikasi.JPG";
import worker from "./assets/img/worker.jpg";
import skillmath from "./assets/img/skillmath.jpg";
import JM from "./assets/img/JM.png";
import SPGroup from './assets/img/SPGroup.webp'

export default function Projects() {
  return (
    <div>
      <section className="projects-section bg-light" id="projects">
        <div className="container">
          <h1 className="text-center">Pengalaman Bekerja</h1>

          <br />
          <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
            <div className="col-lg-6">
              <img className="img-fluid" src={SPGroup} alt="Singapore Power" />
            </div>
            <div className="col-lg-6">
              <div className="bg-black text-center h-100 project">
                <div className="d-flex h-100">
                  <div className="project-text w-100 my-auto text-center text-lg-left">
                    <h4 className="text-white">
                      PT. beIT Inovasi Tiwikrama
                    </h4>
                    <p className="mb-0 text-white-50">
                      ReactJS, VueJS, Tailwind, MUI, Axios, Etc (FrontEnd)
                    </p>
                    <p className="mb-0 text-white-50">
                      Membuat dan mengembangkan Aplikasi Milik Singapore Power, Switch US, SRN MNLHK, SRN PT.BAE, Sampoerna Academy
                    </p>
                    <p></p>
                    <hr className="d-none d-lg-block mb-0 ml-0" />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <hr />
          <div className="row justify-content-center no-gutters">
            <div className="col-lg-6">
              <img className="img-fluid" src={JM} alt="PT.Jasamarga JM-Click" />
            </div>
            <div className="col-lg-6 order-lg-first">
              <div className="bg-black text-center h-100 project">
              <div className="d-flex h-100">
                  <div className="project-text w-100 my-auto text-center text-lg-left">
                    <h4 className="text-white">
                      PT. Hadejun Simedic Indonesia
                    </h4>
                    <p className="mb-0 text-white-50">
                      ReactJS,Tailwind,MUI,Axios,Etc (FrontEnd)
                    </p>
                    <p className="mb-0 text-white-50">
                      Mengembangkankan Aplikasi Milik PT.JasaMarga Persero
                      (BUMN)
                    </p>
                    <p></p>
                    <hr className="d-none d-lg-block mb-0 ml-0" />
                  </div>
                </div>
              </div>
            </div>
          </div>

         

          <hr />
          <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
            <div className="col-lg-6">
              <img className="img-fluid" src={skillmath} alt="..." />
            </div>
            <div className="col-lg-6">
              <div className="bg-black text-center h-100 project">
                <div className="d-flex h-100">
                  <div className="project-text w-100 my-auto text-center text-lg-left">
                    <h4 className="text-white">PT. Yutama Kreasindo</h4>
                    <p className="mb-0 text-white-50">
                      PHP 7, MySql, CodeIgniter 3.
                    </p>
                    <p className="mb-0 text-white-50">
                      Mengembangkankan Aplikasi Skillmath.
                    </p>
                    <hr className="d-none d-lg-block mb-0 ml-0" />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <hr />
          <div className="row justify-content-center no-gutters">
            <div className="col-lg-6">
              <img className="img-fluid" src={worker} alt="..." />
            </div>
            <div className="col-lg-6 order-lg-first">
              <div className="bg-black text-center h-100 project">
                <div className="d-flex h-100">
                  <div className="project-text w-100 my-auto text-center text-lg-right">
                    <h4 className="text-white">
                      Guru & Operator Tata Usaha (Staff Sekolah)
                    </h4>
                    <p className="mb-0 text-white-50">
                      Bekerja Di Sekolah Menengah Pertama (SMP) Sebagai Operator
                      Tata Usaha, dan pernah menjadi guru TIK (Teknologi
                      Informasi Komunikasi) Tahun Ajaran 2020-2021.
                    </p>

                    <hr className="d-none d-lg-block mb-0 mr-0" />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <hr />
          <div className="row justify-content-center no-gutters mb-5 mb-lg-0">
            <div className="col-lg-6">
              <img className="img-fluid" src={logo2} alt="..." />
            </div>
            <div className="col-lg-6">
              <div className="bg-black text-center h-100 project">
                <div className="d-flex h-100">
                  <div className="project-text w-100 my-auto text-center text-lg-left">
                    <h4 className="text-white">Keahlian</h4>
                    <p className="mb-0 text-white-50">
                      Memiliki Keahlian Di Bidang Problem Solving & Problem
                      solving pada komputer dan jaringan
                    </p>
                    <hr className="d-none d-lg-block mb-0 ml-0" />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row justify-content-center no-gutters">
            <div className="col-lg-6">
              <img className="img-fluid" src={logo3} alt="..." />
            </div>
            <div className="col-lg-6 order-lg-first">
              <div className="bg-black text-center h-100 project">
                <div className="d-flex h-100">
                  <div className="project-text w-100 my-auto text-center text-lg-right">
                    <h4 className="text-white">Sertifikasi</h4>
                    <p className="mb-0 text-white-50">
                      Sertifikasi CCNA (Cisco Network Academy).
                    </p>
                    <p className="mb-0 text-white-50">
                      Tentang Pengenalan Jaringan Dan Konfigurasi Pada Server
                    </p>
                    <hr className="d-none d-lg-block mb-0 mr-0" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
