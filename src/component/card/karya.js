import React from "react";
import "./karya.scss";
import karya1 from "../assets/img/karya1.jpg";
import karya3 from "../assets/img/karya3.jpg";
import karya4 from "../assets/img/karya4.JPG";
import karyavpn from "../assets/img/vpn.JPG";
import Cashpier from "../assets/img/figma.png";
import LunaProject from "../assets/img/luna_project.png"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";

function Karya() {
  return (
    <div className="opened" id="karya">
      <section className="dark">
        <div className="container py-4">
          <h1 className="h1 text-center" id="pageHeaderTitle">
            Karya Saya
          </h1>

          <article className="postcard dark blue">
            <a className="postcard__img_link">
              <img className="postcard__img" src={LunaProject} alt="Image Title" />
            </a>
            <div className="postcard__text">
              <h1 className="postcard__title blue">
                <a>Luna Projects (Text To Image)</a>
              </h1>
              <div className="postcard__subtitle small">
                <time dateTime="2020-05-25 12:00:00">
                  <i className="fas fa-calendar-alt mr-2"></i>Desember 2022
                </time>
              </div>
              <div className="postcard__bar"></div>
              <div className="postcard__preview-txt">
                Dibuat Untuk Menghubungkan Project A.I Dengan Web. Peran Saya
                Adalah Sebagai FullStack Javascript. Projek Ini Dibuat Menggunakan Stack MERN
                (MongoDB,ExpressJS,ReactJS,NodeJS)
              </div>
              <ul className="postcard__tagbox">
                <li className="tag__item">
                  <i className="fas fa-tag mr-2"></i>Mern Stack
                </li>
                <li className="tag__item">
                  <i className="fas fa-clock mr-2"></i>
                  <a
                    className="mx-2 mb-5"
                    title="API / ExpressJS"
                    href="https://gitlab.com/nu_vandalstik/mern_api/-/commits/master"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faGitlab} />
                  </a>
                </li>
                <li className="tag__item play blue">
                  <a
                    className="mx-2 mb-5"
                    title="ReactJS"
                    href="https://gitlab.com/nu_vandalstik/mern_blog"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faGitlab} />
                  </a>
                </li>
              </ul>
            </div>
          </article>

          <article className="postcard dark red">
            <a className="postcard__img_link">
              <img className="postcard__img" src={Cashpier} alt="Image Title" />
            </a>
            <div className="postcard__text">
              <h1 className="postcard__title red">
                <a>Aplikasi Kasir (holding) </a>
              </h1>
              <div className="postcard__subtitle small">
                <time dateTime="2020-05-25 12:00:00">
                  <i className="fas fa-calendar-alt mr-2"></i>April 2022
                </time>
              </div>
              <div className="postcard__bar"></div>
              <div className="postcard__preview-txt">
                Aplikasi Kasir Untuk Membantu Usaha UMKM Dalam Melakukan
                Transaksi. Sebagai BackEnd Dan Sisi DevOps.
              </div>
              <ul className="postcard__tagbox">
                <li className="tag__item">
                  <i className="fas fa-tag mr-2"></i>Freelance
                </li>
                <li className="tag__item">
                  <i className="fas fa-clock mr-2"></i>UMKM
                </li>
                <li className="tag__item play red">
                  <a>
                    <i className="fas fa-play mr-2"></i>Private Projects
                  </a>
                </li>
              </ul>
            </div>
          </article>

          <article className="postcard dark blue">
            <a className="postcard__img_link">
              <img className="postcard__img" src={karya1} alt="Image Title" />
            </a>
            <div className="postcard__text">
              <h1 className="postcard__title blue">
                <a>MERN BLOG</a>
              </h1>
              <div className="postcard__subtitle small">
                <time dateTime="2020-05-25 12:00:00">
                  <i className="fas fa-calendar-alt mr-2"></i>Juni 2021.
                </time>
              </div>
              <div className="postcard__bar"></div>
              <div className="postcard__preview-txt">
                Web Blog ini dibuat menggunakan MongoDB, ExpressJS, ReactJs,
                Nodejs, Axios, react-redux, react-router-dom, redux,
                redux-thunk, sass/scss, Express Validator, body-parser, cors,
                mongoose, multer{" "}
              </div>
              <ul className="postcard__tagbox">
                <li className="tag__item">
                  <i className="fas fa-tag mr-2"></i>Mern Stack
                </li>
                <li className="tag__item">
                  <i className="fas fa-clock mr-2"></i>
                  <a
                    className="mx-2 mb-5"
                    title="API / ExpressJS"
                    href="https://gitlab.com/nu_vandalstik/mern_api/-/commits/master"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faGitlab} />
                  </a>
                </li>
                <li className="tag__item play blue">
                  <a
                    className="mx-2 mb-5"
                    title="ReactJS"
                    href="https://gitlab.com/nu_vandalstik/mern_blog"
                    target="_blank"
                  >
                    <FontAwesomeIcon icon={faGitlab} />
                  </a>
                </li>
              </ul>
            </div>
          </article>

          <article className="postcard light blue">
            <a className="postcard__img_link">
              <img className="postcard__img" src={karyavpn} alt="Image Title" />
            </a>
            <div className="postcard__text t-dark">
              <h1 className="postcard__title blue">
                <a className="ml-5">VPN (React Native)</a>
              </h1>
              <div className="postcard__subtitle small ml-5">
                <time dateTime="2020-05-25 12:00:00">
                  <i className="fas fa-calendar-alt mr-2"></i>-
                </time>
              </div>
              <div className="postcard__bar ml-5"></div>
              <div className="postcard__preview-txt ml-5">
                VPN ini dibuat menggunakan React native dengan dependencies
                @react-native-picker/picker, axios, react-native-elements,
                react-native-ip-sec-vpn, react-native-safe-area-context,
                react-native-vector-icons. dan aplikasi ini dilengkapi dengan
                iklan Google ADMOB.
              </div>
              <ul className="postcard__tagbox">
                <li className="tag__item">
                  <i className="fas fa-tag mr-2"></i>Private Projects
                </li>
                <li className="tag__item">
                  <i className="fas fa-clock mr-2"></i> VPN
                </li>
                <li className="tag__item play blue">
                  <a>
                    <i className="fas fa-play mr-2"></i>Virtual Private Network
                  </a>
                </li>
              </ul>
            </div>
          </article>

          <article className="postcard dark red">
            <a className="postcard__img_link">
              <img className="postcard__img" src={karya3} alt="Image Title" />
            </a>
            <div className="postcard__text">
              <h1 className="postcard__title red">
                <a>Persuratan Kodiklatal Pusdiklek </a>
              </h1>
              <div className="postcard__subtitle small">
                <time dateTime="2020-05-25 12:00:00">
                  <i className="fas fa-calendar-alt mr-2"></i>Desember 2021
                </time>
              </div>
              <div className="postcard__bar"></div>
              <div className="postcard__preview-txt">
                Pembuatan Aplikasi Persuratan. Aplikasi ini dibuat dengan
                Laravel 8, Php7, Mysqli / PhpmyAdmin / MariaDB
              </div>
              <ul className="postcard__tagbox">
                <li className="tag__item">
                  <i className="fas fa-tag mr-2"></i>Freelance
                </li>
                <li className="tag__item">
                  <i className="fas fa-clock mr-2"></i>Persuratan
                </li>
                <li className="tag__item play red">
                  <a>
                    <i className="fas fa-play mr-2"></i>Private Projects
                  </a>
                </li>
              </ul>
            </div>
          </article>

          <article className="postcard light blue">
            <a
              className="postcard__img_link"
              href="https://friendly-wilson-b0e676.netlify.app/"
            >
              <img className="postcard__img" src={karya4} alt="Image Title" />
            </a>
            <div className="postcard__text t-dark">
              <h1 className="postcard__title blue">
                <a
                  href="https://friendly-wilson-b0e676.netlify.app/"
                  className="ml-5"
                >
                  Vanilla JavaScript
                </a>
              </h1>
              <div className="postcard__subtitle small ml-5">
                <time dateTime="2020-05-25 12:00:00">
                  <i className="fas fa-calendar-alt mr-2"></i>-
                </time>
              </div>
              <div className="postcard__bar ml-5"></div>
              <div className="postcard__preview-txt ml-5">
                Web ini adalah hasil belajar saya Menggunakan VanillaJS,
                menggunakan Axios, dan belajar tentang Asyncronus dan Syncronus.
              </div>
              <ul className="postcard__tagbox">
                <li className="tag__item">
                  <i className="fas fa-tag mr-2"></i>VanillajS
                </li>
                <li className="tag__item">
                  <i className="fas fa-clock mr-2"></i> JavaScript
                </li>
                <li className="tag__item play blue">
                  <a href="https://friendly-wilson-b0e676.netlify.app/">
                    <i className="fas fa-play mr-2"></i>Learning.
                  </a>
                </li>
              </ul>
            </div>
          </article>

          {/* <article class="postcard dark green">
            <a class="postcard__img_link" href="#">
              <img
                class="postcard__img"
                src="https://picsum.photos/500/501"
                alt="Image Title"
              />
            </a>
            <div class="postcard__text">
              <h1 class="postcard__title green">
                <a href="#">Podcast Title</a>
              </h1>
              <div class="postcard__subtitle small">
                <time dateTime="2020-05-25 12:00:00">
                  <i class="fas fa-calendar-alt mr-2"></i>Mon, May 25th 2020
                </time>
              </div>
              <div class="postcard__bar"></div>
              <div class="postcard__preview-txt">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, fugiat asperiores inventore beatae accusamus odit
                minima enim, commodi quia, doloribus eius! Ducimus nemo
                accusantium maiores velit corrupti tempora reiciendis molestiae
                repellat vero. Eveniet ipsam adipisci illo iusto quibusdam, sunt
                neque nulla unde ipsum dolores nobis enim quidem excepturi,
                illum quos!
              </div>
              <ul class="postcard__tagbox">
                <li class="tag__item">
                  <i class="fas fa-tag mr-2"></i>Podcast
                </li>
                <li class="tag__item">
                  <i class="fas fa-clock mr-2"></i>55 mins.
                </li>
                <li class="tag__item play green">
                  <a href="#">
                    <i class="fas fa-play mr-2"></i>Play Episode
                  </a>
                </li>
              </ul>
            </div>
          </article> */}
          {/* <article class="postcard dark yellow">
            <a class="postcard__img_link" href="#">
              <img
                class="postcard__img"
                src="https://picsum.photos/501/501"
                alt="Image Title"
              />
            </a>
            <div class="postcard__text">
              <h1 class="postcard__title yellow">
                <a href="#">Podcast Title</a>
              </h1>
              <div class="postcard__subtitle small">
                <time dateTime="2020-05-25 12:00:00">
                  <i class="fas fa-calendar-alt mr-2"></i>Mon, May 25th 2020
                </time>
              </div>
              <div class="postcard__bar"></div>
              <div class="postcard__preview-txt">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Eligendi, fugiat asperiores inventore beatae accusamus odit
                minima enim, commodi quia, doloribus eius! Ducimus nemo
                accusantium maiores velit corrupti tempora reiciendis molestiae
                repellat vero. Eveniet ipsam adipisci illo iusto quibusdam, sunt
                neque nulla unde ipsum dolores nobis enim quidem excepturi,
                illum quos!
              </div>
              <ul class="postcard__tagbox">
                <li class="tag__item">
                  <i class="fas fa-tag mr-2"></i>Podcast
                </li>
                <li class="tag__item">
                  <i class="fas fa-clock mr-2"></i>55 mins.
                </li>
                <li class="tag__item play yellow">
                  <a href="#">
                    <i class="fas fa-play mr-2"></i>Play Episode
                  </a>
                </li>
              </ul>
            </div>
          </article> */}
        </div>
      </section>
    </div>
  );
}

export default Karya;
