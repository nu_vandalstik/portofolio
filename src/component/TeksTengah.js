// import './style/css/styles.css'
// import './style/js/scripts'
import "bootstrap/dist/css/bootstrap.css";
import "./styling.css";
import nu from "./assets/img/nu.jpg";
// import nuu from './assets/img/nuu.jpg'

export default function TeksTengah(props) {
  const { nama } = props;
  const keahlian = ["MERN STACK", "IT SUPPORT", "FULLSTACK DEVELOPERS"];
  // let ahli = keahlian.map((itemAhli)=> itemAhli)
  const ahli = keahlian.join(", ");
  return (
    <div>
      <header className="masthead mt-5">
        <div className="container ">
          <div className="mx-auto text-center">
            {/* <h1 className="mx-auto my-0 text-uppercase">{nama}</h1> */}

            <div className="d-flex justify-content-center">
              ...
              <div className="besar d-flex justify-content-center">
                <img
                  src={nu}
                  className="card-img-top gambar  top "
                  alt="..."
                ></img>
              </div>
            </div>
            <h2 className="text-white-50 mx-auto  mb-3 fs-2">{nama}</h2>
            <h2 className="text-white-50 mx-auto mt-5 mb-5">{ahli}</h2>
            <a className="btn btn-primary js-scroll-trigger" href="#about">
              Yuk Lihat
            </a>
          </div>
        </div>
      </header>
    </div>
  );
}
