import "../App.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faGitlab,
  faInstagramSquare,
} from "@fortawesome/free-brands-svg-icons";
// import { library } from "@fortawesome/fontawesome-svg-core";
// import { faCodeBranch } from "@fortawesome/free-solid-svg-icons";

export default function Footer() {
  return (
    <div>
      <h3 className="container text-center">
        {" "}
        Masih Banyak karya Lainya. Tunggu ya :){" "}
      </h3>
      <section className="contact-section bg-black" id="footer">
        <div className="container">
          <div className="row">
            <div className="col-md-4 mb-3 mb-md-0">
              <div className="card py-4 h-100">
                <div className="card-body text-center">
                  <i className="fas fa-map-marked-alt text-primary mb-2"></i>
                  <h4 className="text-uppercase m-0">Address</h4>
                  <hr className="my-4 container" />
                  <div className="small text-dark">Kota Bogor</div>
                </div>
              </div>
            </div>
            <div className="col-md-4 mb-3 mb-md-0">
              <div className="card py-4 h-100">
                <div className="card-body text-center">
                  <i className="fas fa-envelope text-primary mb-2"></i>
                  <h4 className="text-uppercase m-0">Email</h4>
                  <hr className="my-4 container" />
                  <div className="small text-dark">
                    <a href="#!">wisnukristanto321@gmail.com</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4 mb-3 mb-md-0">
              <div className="card py-4 h-100">
                <div className="card-body text-center">
                  <i className="fas fa-mobile-alt text-primary mb-2"></i>
                  <h4 className="text-uppercase m-0">Phone</h4>
                  <hr className="my-4 container" />
                  <div className="small text-dark">+6283811838345</div>
                </div>
              </div>
            </div>
          </div>

          <div className="social d-flex justify-content-center ">
            <a
              className="mx-2 mb-5"
              href="https://www.instagram.com/nu_vandalstik/"
              target="_blank"
            >
              <FontAwesomeIcon icon={faInstagramSquare} />{" "}
            </a>
            <a
              className="mx-2 mb-5"
              href="https://www.facebook.com/uzBlackHwk/"
              target="_blank"
            >
              <FontAwesomeIcon icon={faFacebook} />
            </a>
            <a
              className="mx-2 mb-5"
              href="https://gitlab.com/nu_vandalstik"
              target="_blank"
            >
              <FontAwesomeIcon icon={faGitlab} />
            </a>
          </div>
        </div>
      </section>
    </div>
  );
}
