import "./App.css";
import Navbar from "./component/Navbar";
import TeksTengah from "./component/TeksTengah";
import "./component/TeksTengah";
import Bagian2 from "./component/Bagian2";
import Projects from "./component/Projects";
import Footer from "./component/Footer";
import Karya from "./component/card/karya";

function App() {
  document.title = "Portofolio Wisnu";
  return (
    <div className="App">
      <Navbar judul="Portofolio"></Navbar>
      <TeksTengah nama="Wisnu Kristanto"></TeksTengah>
      <Bagian2 siapa="Saya Seorang Fullstack Javascript, Fokus saya dibidang ini adalah MERN."></Bagian2>
      <Projects></Projects>
      <Karya />
      <Footer></Footer>
    </div>
  );
}

export default App;
